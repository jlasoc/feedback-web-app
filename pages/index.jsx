import React from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import { Footer } from '../src/Layout/Footer';
import { Header } from '../src/Layout/Header';
import { MainContent } from '../src/Assets/MainContent';
import { PersonalContent } from '../src/components/PersonalContent';

const useStyles = makeStyles((theme) => ({
  heroContent: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(8, 0, 6),
  },
  cardGrid: {
    paddingTop: theme.spacing(8),
    paddingBottom: theme.spacing(8),
  },
  footer: {
    backgroundColor: theme.palette.primary.main,
    color: theme.palette.primary.contrastText,
    padding: theme.spacing(2),
  },
}));


export default function FeedbackCardIndex() {
  const classes = useStyles();

  return (
    <>
      <CssBaseline />
      <Header />
      <main>
        <div className={classes.heroContent}>
          < MainContent />
        </div>
        <Container className={classes.cardGrid} maxWidth="md">
          <PersonalContent />
        </Container>
      </main>
      <footer className={classes.footer}>
        <Footer />
      </footer>
    </>
  );
}
import React from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import { makeStyles } from '@material-ui/core/styles';
import { Footer } from '../src/Layout/Footer';
import { Header } from '../src/Layout/Header';
import Paper from '@material-ui/core/Paper';
import {
  Chart,
  PieSeries,
  Title,
} from '@devexpress/dx-react-chart-material-ui';
import Typography from '@material-ui/core/Typography';
import { Animation } from '@devexpress/dx-react-chart';



const useStyles = makeStyles((theme) => ({
  heroContent: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(8, 0, 6),
    height: '100vh'
  },
  footer: {
    backgroundColor: theme.palette.primary.main,
    color: theme.palette.primary.contrastText,
    padding: theme.spacing(2),
  },
}));

const data = [
  { country: 'Russia', area: 12 },
  { country: 'Canada', area: 7 },
  { country: 'USA', area: 7 },
  { country: 'China', area: 7 },
  { country: 'Brazil', area: 6 },
  { country: 'Australia', area: 5 },
  { country: 'India', area: 2 },
  { country: 'Others', area: 55 },
];

export default function DataVisualization() {
  const classes = useStyles();

  return (
    <>
      <CssBaseline />
      <Header />
      <main>
        <div className={classes.heroContent}>
          <div>
            <h1>Hola explicación</h1>
          </div>
          <div>
            <Paper elevation={0}>
              <Chart
                data={data}
              >
                <PieSeries
                  valueField="area"
                  argumentField="country" primary
                />
                <Title
                  text="Areas to workout"
                />
                <Animation />
              </Chart>
            </Paper>
          </div>
        </div>
      </main>
      <footer className={classes.footer}>
        <Footer />
      </footer>
    </>
  );
}
import React from 'react';
import Container from '@material-ui/core/Container';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';


const useStyles = makeStyles((theme) => ({
	heroButtons: {
		marginTop: theme.spacing(4),
	},
}));

const MainContent = () => {
	const classes = useStyles();

	return (
		<Container maxWidth="sm">
			<Typography component="h1" variant="h2" align="center" color="textPrimary" gutterBottom>
				Team Cards
          </Typography>
			<Typography variant="h5" align="center" color="textSecondary" paragraph>
				Esta página muestra a todos los miembos del equipo y un link con un form para feedback,
				cada persona debe proveer su propio formulario, de modo que los feedbacks solo los reciba la persona
				que disponibilizó el formulario.
          </Typography>
			<div className={classes.heroButtons}>
				<Grid container spacing={2} justify="center">
					<Grid item>
						<Button variant="contained" color="primary">
							How to do a feedback form?
                </Button>
					</Grid>
					<Grid item>
						<Button variant="outlined" color="primary">
							¿How to give feedback?
                </Button>
					</Grid>
				</Grid>
			</div>
		</Container>
	)
};

export { MainContent };
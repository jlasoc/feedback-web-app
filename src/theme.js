import { createMuiTheme } from '@material-ui/core/styles';
import { red } from '@material-ui/core/colors';

// Create a theme instance.
const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#9c27b0',
    },
    secondary: {
      main: '#009688',
    },
    error: {
      main: red.A400,
    },
    background: {
      default: '#eeeeee',
    },
  },
});

export default theme;

import React from "react";
import { Grid } from "@material-ui/core";
import cardConstants from '../Assets/cardConstants';
import { FeedbackCard } from '../components/FeedbackCard';

const PersonalContent = () => {
  const getTeamMemberCard = teamCardObj => {
    return (
      <Grid item xs={12} sm={8} md={4}>
        <FeedbackCard {...teamCardObj} />
      </Grid>
    );
  };

  return (
    <Grid container spacing={4}>
      {cardConstants.map(teamCardObj => 
        getTeamMemberCard(teamCardObj))}
    </Grid>
  );
};

export { PersonalContent };
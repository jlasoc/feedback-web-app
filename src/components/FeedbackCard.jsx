import React from 'react';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';



const useStyles = makeStyles((theme) => ({
	card: {
		height: '100%',
		display: 'flex',
		flexDirection: 'column',
	},
	cardMedia: {
		paddingTop: '56.25%', // 16:9
	},
	cardContent: {
		flexGrow: 1,
	},

}));

const FeedbackCard = (props) => {
	const classes = useStyles();
	const { title, description, imageUrl, id } = props;

	return (
		<>
			<Grid container>
						<Card className={classes.card}>
							<CardMedia
								className={classes.cardMedia}
								image={imageUrl}
								title={title}
							/>
							<CardContent className={classes.cardContent}>
								<Typography gutterBottom variant="h5" component="h2">
									{title}
                </Typography>
								<Typography variant="body2" component="p">
									{description}
                </Typography>
							</CardContent>
							<CardActions>
								<Button size="small" color="primary">
									View
                </Button>
							</CardActions>
						</Card>
			</Grid>
		</>
	)
};

export { FeedbackCard };
import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import PeopleIcon from '@material-ui/icons/People';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  icon: {
    marginRight: theme.spacing(2),
  },
}));

const Header = () => {
  const classes = useStyles();
  
  return (
    <>
    <AppBar position="relative">
        <Toolbar>
          <PeopleIcon className={classes.icon} />
          <Typography variant="h6" color="inherit" noWrap>
            Feedback Web Tracker and Sharing
          </Typography>
        </Toolbar>
      </AppBar>
    </>
  )
};

export { Header };
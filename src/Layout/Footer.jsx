import React from 'react';
import Typography from '@material-ui/core/Typography';
import { Copyright } from './Copyright';


const Footer = () => {
	return (
		<>
			<Typography variant="h6" align="center" gutterBottom>
				Footer
      </Typography>
			<Typography variant="subtitle1" align="center" color="initial" component="p">
				Something here to give the footer a purpose!
      </Typography>
			<Copyright />
		</>
	)
};

export { Footer };